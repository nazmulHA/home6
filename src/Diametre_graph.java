// Java program to find diameter of a 
// graph using DFS.

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class Diametre_graph {
    
    static int x;
    static int maxCount;
    static List<Integer> adj[];
     
    
    static void dfsUtil(int node, int count, 
                         boolean visited[],
                       List<Integer> adj[])
    {
        visited[node] = true;
        count++;
         
        List<Integer> l = adj[node];
        for(Integer i: l)
        {
            if(!visited[i]){
                if (count >= maxCount) {
                    maxCount = count;
                    x = i;
                }
                dfsUtil(i, count, visited, adj);
            }
        }
    }
      
   
    static void dfs(int node, int n, List<Integer>
                                       adj[])
    {
        boolean[] visited = new boolean[n + 1];
        int count = 0;
      
      
        Arrays.fill(visited, false);
      
      
        dfsUtil(node, count + 1, visited, adj);
         
    }
      
    
    static int diameter(List<Integer> adj[], int n)
    {
        maxCount = Integer.MIN_VALUE;
     
        dfs(1, n, adj);
      
        
        dfs(x, n, adj);
      
        return maxCount;
    }
      
    
    public static void main(String args[])
    {
        int n = 5;
      
      
        adj = new List[n + 1];
        for(int i = 0; i < n+1 ; i++)
            adj[i] = new ArrayList<Integer>(); 
      
        /*create undirected edges */
        adj[1].add(2);
        adj[2].add(1);
        adj[1].add(3);
        adj[3].add(1);
        adj[2].add(4);
        adj[4].add(2);
        adj[2].add(5);
        adj[5].add(2);
         
       
        System.out.println("Diameter of the given " +
                       "graph is " + diameter(adj, n));
    }
}
